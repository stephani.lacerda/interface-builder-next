export const menu = [
  {
    id: 1,
    name: 'Conteúdo',
    submenu: [
      {
        id: 1,
        name: 'Home',
        options: [
          'Banner',
          'Destaque',
          'Atalhos',
          'Últimas Atualizações',
          'Resultados',
          'Próximos Eventos',
          'Quadro de Resultados',
          'Quadro da Assembléia'
        ]
      },
      {
        id: 2,
        name: 'Páginas',
        options: [
          'Editorial',
          'Arquivos',
          'Agenda',
          'Resultados',
          'Cadastro',
          'Fale com RI',
          'Grafico e Cotações',
          'Proventos',
          'Tabelas'
        ]
      },
      {
        id: 3,
        name: 'Documentos',
        options: []
      },
      {
        id: 4,
        name: 'Estatísticas',
        options: []
      }
    ]
  },
  {
    id: 2,
    name: 'Interação',
    submenu: [
      {
        id: 1,
        name: 'Mala Direta',
        options: []
      },
      {
        id: 2,
        name: 'Alertas',
        options: []
      },
      {
        id: 3,
        name: 'Cadastros',
        options: []
      },      
    ]
  },
  {
    id: 3,
    name: 'Configurações',
    submenu: [
      {
        id: 1,
        name: 'Layout',
        options: []
      },
      {
        id: 2,
        name: 'Mapeamento',
        options: []
      },
      {
        id: 3,
        name: 'Atividade',
        options: []
      },      
    ]
  }
]