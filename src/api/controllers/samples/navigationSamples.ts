export const getNavigationResponse = {
	success: true,
	data: [
		// A Empresa
		{
			areaId: 1,
			areaUuid: 'a7f8d7ee-91c5-42e4-a2b9-7b66404a6f87',
			areaTitle: 'A Empresa',
			areaSlug: 'a-empresa',
			L10n: {
				pt: {
					areaTitle: 'A Empresa',
					areaSlug: 'a-empresa',
				},
				en: {
					areaTitle: 'Company',
					areaSlug: 'company',
				},
			},
			pages: [
				{
					pageId: 1,
					pageUuid: 'ccd79a02-cebb-4df3-a59f-8d068390ab3d',
					pageTitle: 'Modelo de Negócios',
					pageSlug: 'modelo-de-negocios',
					pageType: 'editorial',
					L10n: {
						pt: {
							pageTitle: 'Modelo de Negócios',
							pageSlug: 'modelo-de-negocios',
							pageUrl: 'https://investidores.emitremmus.com/a-empresa/modelo-de-negocios',
						},
						en: {
							pageTitle: 'Business Model',
							pageSlug: 'business-model',
							pageUrl: 'https://investors.emitremmus.com/company/business-model',
						},
					},
				},
				{
					pageId: 2,
					pageUuid: '84ad287b-b76b-42b5-9333-0aca7f7f05ff',
					pageTitle: 'Sustentabilidade',
					pageSlug: 'sustentabilidade',
					pageType: 'editorial',
					L10n: {
						pt: {
							pageTitle: 'Sustentabilidade',
							pageSlug: 'sustentabilidade',
							pageUrl: 'https://investidores.emitremmus.com/a-empresa/sustentabilidade',
						},
						en: {
							pageTitle: 'Sustainability',
							pageSlug: 'sustainability',
							pageUrl: 'https://investors.emitremmus.com/company/sustainability',
						},
					},
				},
				{
					pageId: 3,
					pageUuid: '0cb1eaff-b4d4-4a04-8bad-6354eac47d91',
					pageTitle: 'Nossa História',
					pageSlug: 'nossa-historia',
					pageType: 'editorial',
					L10n: {
						pt: {
							pageTitle: 'Nossa História',
							pageSlug: 'nossa-historia',
							pageUrl: 'https://investidores.emitremmus.com/a-empresa/nossa-historia',
						},
						en: {
							pageTitle: 'Our History',
							pageSlug: 'our-history',
							pageUrl: 'https://investors.emitremmus.com/company/our-history',
						},
					},
				},
			],
		},

		// Governança Corporativa
		{
			areaId: 2,
			areaUuid: 'fca6bfa6-2b3f-4c38-979f-53592c94628',
			areaTitle: 'Governança Corporativa',
			areaSlug: 'governanca-corporativa',
			L10n: {
				pt: {
					areaTitle: 'Governança Corporativa',
					areaSlug: 'governanca-corporativa',
				},
				en: {
					areaTitle: 'Corporate Governance',
					areaSlug: 'corporate-governance',
				},
			},
			pages: [
				{
					pageId: 4,
					pageUuid: 'c49210e9-ff4f-4acf-8e1e-348fb5e95dfd',
					pageTitle: 'Estrutura Societária',
					pageSlug: 'estrutura-societaria',
					pageType: 'editorial',
					L10n: {
						pt: {
							pageTitle: 'Estrutura Societária',
							pageSlug: 'estrutura-societaria',
							pageUrl:
								'https://investidores.emitremmus.com/goveranca-corporativa/estrutura-societaria',
						},
						en: {
							pageTitle: 'Ownership Structure',
							pageSlug: 'ownership-structure',
							pageUrl: 'https://investors.emitremmus.com/corporate-governance/ownership-structure',
						},
					},
				},
				{
					pageId: 5,
					pageUuid: 'cef949f5-2fbb-4dc0-bb1a-8ed17bde82c2',
					pageTitle: 'Diretoria',
					pageSlug: 'diretoria',
					pageType: 'editorial',
					L10n: {
						pt: {
							pageTitle: 'Diretoria',
							pageSlug: 'diretoria',
							pageUrl: 'https://investidores.emitremmus.com/goveranca-corporativa/diretoria',
						},
						en: {
							pageTitle: 'Executive Officers',
							pageSlug: 'executive-officers',
							pageUrl: 'https://investors.emitremmus.com/corporate-governance/executive-officers',
						},
					},
				},
				{
					pageId: 6,
					pageUuid: 'd79d35ff-6058-454f-8035-e293ab036b93',
					pageTitle: 'Conselho e Comitês',
					pageSlug: 'conselho-e-comites',
					pageType: 'editorial',
					L10n: {
						pt: {
							pageTitle: 'Conselho e Comitês',
							pageSlug: 'conselho-e-comites',
							pageUrl:
								'https://investidores.emitremmus.com/goveranca-corporativa/conselho-e-comites',
						},
						en: {
							pageTitle: 'Board and Committees',
							pageSlug: 'board-and-committees',
							pageUrl: 'https://investors.emitremmus.com/corporate-governance/board-and-committees',
						},
					},
				},
				{
					pageId: 7,
					pageUuid: '7ea7d446-98fa-4b11-9cdf-d0fcee912b14',
					pageTitle: 'Atas de Reuniões',
					pageSlug: 'atas-de-reunioes',
					pageType: 'editorial',
					L10n: {
						pt: {
							pageTitle: 'Atas de Reuniões',
							pageSlug: 'atas-de-reunioes',
							pageUrl: 'https://investidores.emitremmus.com/goveranca-corporativa/atas-de-reunioes',
						},
						en: {
							pageTitle: 'Meeting Minutes',
							pageSlug: 'meeting-minutes',
							pageUrl: 'https://investors.emitremmus.com/corporate-governance/meeting-minutes',
						},
					},
				},
				{
					pageId: 8,
					pageUuid: '6e94ec9d-dc92-452a-aaa7-6b783a6d20f9',
					pageTitle: 'Estatutos e Políticas',
					pageSlug: 'estatutos-e-politicas',
					pageType: 'editorial',
					L10n: {
						pt: {
							pageTitle: 'Estatutos e Políticas',
							pageSlug: 'estatutos-e-politicas',
							pageUrl:
								'https://investidores.emitremmus.com/goveranca-corporativa/estatutos-e-politicas',
						},
						en: {
							pageTitle: 'Bylaws and Policies',
							pageSlug: 'bylaws-and-policies',
							pageUrl: 'https://investors.emitremmus.com/corporate-governance/bylaws-and-policies',
						},
					},
				},
			],
		},

		// Informações Financeiras
		{
			areaId: 3,
			areaUuid: 'ad9641c6-ba36-4ad0-b840-8593d78732d2',
			areaTitle: 'Informações Financeiras',
			areaSlug: 'informacoes-financeiras',
			L10n: {
				pt: {
					areaTitle: 'Informações Financeiras',
					areaSlug: 'informacoes-financeiras',
				},
				en: {
					areaTitle: 'Financial Information',
					areaSlug: 'financial-information',
				},
			},
			pages: [],
		},

		// Publicações
		{
			areaId: 4,
			areaUuid: '0cbcba52-fa67-4e02-81dc-846b147ec9cb',
			areaTitle: 'Publicações',
			areaSlug: 'publicacoes',
			L10n: {
				pt: {
					areaTitle: 'Publicações',
					areaSlug: 'publicacoes',
				},
				en: {
					areaTitle: 'Publications',
					areaSlug: 'publications',
				},
			},
			pages: [],
		},

		// Serviços ao Investidor
		{
			areaId: 5,
			areaUuid: 'd0c9b05a-3a68-421e-983e-c92fcd9eea4d',
			areaTitle: 'Serviços ao Investidor',
			areaSlug: 'servicos-ao-investidor',
			L10n: {
				pt: {
					areaTitle: 'Serviços ao Investidor',
					areaSlug: 'servicos-ao-investidor',
				},
				en: {
					areaTitle: 'Investor Services',
					areaSlug: 'investor-services',
				},
			},
			pages: [],
		},
	],
}
